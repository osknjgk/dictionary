#include <string>
#include <curl/curl.h>

using namespace std;

class Curl {
private:
	CURL *hnd;
	string buf;

private:
	// curlに渡すコールバック関数．
	// curlが取得したデータがbufferに格納されており，userbufには先に指定したバッファへのポインタが格納されている．
	// ここでは単純にbufferの中身をuserbufに移し，外部で利用できるようにする．
	static size_t write_data(char *buffer, size_t size, size_t nmemb, string *userbuf);

public:
	// このクラスの利用を開始する前に1度だけ呼ぶ．
	static void init();

	// このクラスを利用し終わり，今後利用することがない場合に1度だけ呼ぶ．
	static void clean();

	// 与えられた文字列をURLエンコーディングして返す．
	string escape(string str);

	// 接続先URLを指定する．
	Curl(string url);

	// 指定したURLからデータを取得する．
	// 戻り値は取得したデータへのポインタ．
	// 内部変数へのポインタを返しているため，何度呼んでも同じポインタが返される．
	string *get();
};

