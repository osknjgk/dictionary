all:
	g++ -g -c -o CurlWrapper.o CurlWrapper.cpp -lcurl
	g++ -g -c -o Dictionary.o Dictionary.cpp -lcurl
	g++ -g -o dic dic.cpp CurlWrapper.o Dictionary.o -lcurl

clean:
	rm -f Dictionary.o
	rm -f CurlWrapper.o
	rm -f dic
