#include <curl/curl.h>
#include "CurlWrapper.hpp"

using namespace std;

size_t Curl::write_data(char *buffer, size_t size, size_t nmemb, string *userbuf) {
	size_t len = size * nmemb;
	userbuf->append(buffer);
	return len;
}

void Curl::init() {
	curl_global_init(CURL_GLOBAL_ALL);
}

void Curl::clean() {
	curl_global_cleanup();
}

string Curl::escape(string str) {
	char *e = curl_easy_escape(hnd, str.c_str(), str.length());
	string estr(e);
	delete e;
	return estr;
}

Curl::Curl(string url) {
	hnd = curl_easy_init();
	curl_easy_setopt(hnd, CURLOPT_URL, url.c_str());
	curl_easy_setopt(hnd, CURLOPT_WRITEFUNCTION, write_data);
	curl_easy_setopt(hnd, CURLOPT_WRITEDATA, &buf);
	//curl_easy_setopt(hnd, CURLOPT_FILE, );
}

string *Curl::get() {
	CURLcode cc = curl_easy_perform(hnd);
	if (cc != CURLE_OK) return NULL;
	return &buf;
}
