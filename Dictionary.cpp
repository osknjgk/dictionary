#include <iostream>
#include <string>
#include <map>
#include "CurlWrapper.hpp"
#include "Dictionary.hpp"

using namespace std;

Dictionary::Dictionary(mode md) {
	this->md = md;
}

int Dictionary::tag_contents(const string &text, const string &bstr, const string &estr, string *contents) {
	int begin_cur = 0, end_cur = 0;

	begin_cur = text.find(bstr);
	if (begin_cur == string::npos) {
		*contents = "";
		return 0;
	}
	begin_cur += bstr.length();
	end_cur = text.find(estr, begin_cur);
	if (end_cur != string::npos) {
		int recurse_tag = text.find(bstr, begin_cur);
		// 入れ子のタグが存在する場合．
		if (recurse_tag != string::npos && recurse_tag < end_cur) {
			string c;
			tag_contents(text.substr(recurse_tag), bstr, estr, &c);
			end_cur = text.find(estr, recurse_tag + c.length() + 1);
		}
		*contents = text.substr(begin_cur, end_cur-begin_cur);
	} else {
		if (begin_cur < text.length()) *contents = text.substr(begin_cur);
		else *contents = "";
	}
	return begin_cur;
}

void Dictionary::search(string word, map<string, string> *suggestion) {
	string req = search_url;
	req = req + "Dic=";
	if (md == Dictionary::ENG2JPN) req = req + e2j;
	else if (md == Dictionary::JPN2ENG) req = req + j2e;
	req = req + "&Word=";
	if (md == Dictionary::ENG2JPN) {
		req = req + word;
	} else if (md == Dictionary::JPN2ENG) {
		Curl c("");
		req = req + c.escape(word);
	}
	req = req + "&Scope=" + "ANYWHERE";
	req = req + "&Match=" + "CONTAIN";		// EXACT/STARTWITH/ENDWITH/CONTAIN
	req = req + "&Merge=" + "AND";
	req = req + "&Prof=" + "XML";
	req = req + "&PageSize=" + "1000";
	req = req + "&PageIndex=" + "0";
	Curl curl(req);
	string s = *curl.get();
	string sub;
	int cur = tag_contents(s, "<DicItemTitle>", "</DicItemTitle>", &sub);
	while(sub.length() > 0) {
		string id, title_sub, title;
		tag_contents(s, "<ItemID>", "</ItemID>", &id);
		tag_contents(s, "<Title>", "</Title>", &title_sub);
		tag_contents(title_sub, "<span class=\"NetDicTitle\" xmlns=\"\">", "</span>", &title);
		suggestion->insert(map<string , string>::value_type(title, id));
		s = s.substr(cur + sub.length());
		cur = tag_contents(s, "<DicItemTitle>", "</DicItemTitle>", &sub);
	}
}

void Dictionary::getitem(string id) {
	string req = getitem_url;
	req = req + "Dic=";
	if (md == Dictionary::ENG2JPN) req = req + e2j;
	else if (md == Dictionary::JPN2ENG) req = req + j2e;
	req = req + "&Item=" + id;
	req = req + "&Loc=" + "";
	req = req + "&Prof=" + "XHTML";
	Curl curl(req);
	string s = *curl.get();
	string c;
	tag_contents(s, "<div>", "</div>", &c);
	cout << c << endl;
}
