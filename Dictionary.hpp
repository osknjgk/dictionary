#include <string>

using namespace std;

class Dictionary {
private:
	const string search_url = "http://public.dejizo.jp/NetDicV09.asmx/SearchDicItemLite?";
	const string getitem_url = "http://public.dejizo.jp/NetDicV09.asmx/GetDicItemLite?";
	const string e2j = "EJdict";
	const string j2e = "EdictJE";

public:
	enum mode {ENG2JPN, JPN2ENG};

private:
	mode md;

public:
	Dictionary(mode md);

	// 指定されたタグで囲まれている文章を取得する．
	// 戻り値は，text内でのcontentsに格納する部分の位置となる．
	int tag_contents(const string &text, const string &bstr, const string &estr, string *contents);

	void search(string word, map<string, string> *suggestion);
	
	void getitem(string id);
};

